﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;

namespace ConTools
{
    public static class ConOut
    {
        public static void Title(object title, ConsoleColor color = ConsoleColor.White)
        {
            Print($"──── {title} ────", color);
        }
        public static void Print(object content = null, ConsoleColor color = ConsoleColor.White, bool newline = true)
        {
            Console.ForegroundColor = color;
            if (newline)
                Console.WriteLine(content);
            else
                Console.Write(content);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void List<T>(IEnumerable<T> list, ConsoleColor color = ConsoleColor.White)
        {
            var l = new List<T>(list);

            var lastIndexCount = DigitCount(l.Count - 1);
            var listTypeString = list.GetType().ToString();
            var listT = listTypeString.Split('.').Last().Replace("]", "");
            var listType = listTypeString.Replace(listTypeString.Split('[').Last(), "").Split('.').Last().Replace("[","").Split('`').First();
            Console.ForegroundColor = color;
            //Print($"─────────────────────", color);
            Print($"{listType}", color, false);
            Print($"<{listT}>", ConsoleColor.Cyan, false);
            Print($"[{l.Count}]", color);
            Print($"─────────────────────", color);
            for (var i = 0; i < l.Count; i++)
            {
                var diff = lastIndexCount - DigitCount(i);
                Print($"{GetStackedString(" ", diff)}{i} | {l[i]}", color);
            }
            Print($"─────────────────────", color);

            Console.ForegroundColor = ConsoleColor.White;
        }

        public static long MeasureTime(Action action, bool print = true)
        {
            var sw = new Stopwatch();
            sw.Start();
            action();
            sw.Stop();
            if (print)
            {
                var t = sw.Elapsed;
                Print($"── Elapsed {t.Hours}:{t.Minutes}:{t.Seconds}.{t.Milliseconds} ({t.TotalMilliseconds} ms)");
            }
            return sw.ElapsedMilliseconds;
        }

        public static void AreEqual(object expected, object actual)
        {
            var eq = expected.Equals(actual);
            Print($"──> AreEqual [{expected} "+ (eq ? "=" : "!") +$" {actual}] {eq}", eq ? ConsoleColor.Green : ConsoleColor.Red);
        }

        private static int DigitCount(int n)
        {
            if (n == 0) return 1;
            return (int)(Math.Floor(Math.Log10(n)) + 1);
        }
        private static string GetStackedString(string str, int count)
        {
            var d = new string[count];
            for (var i = 0; i < count; i++)
            {
                d[i] = str;
            }
            return string.Join("", d);
        }
    }
}
