﻿using System;

namespace ConTools
{
    public static class Num
    {
        //public static int GetDigitCount(long n)
        //{
        //    return n == 0 ? 1 : (int)Math.Floor(Math.Log10(n) + 1);
        //}

        ///// <summary>
        ///// Truncates a number from left side [1234, 234, 34, 4, 0]
        ///// </summary>
        ///// <param name="n">Number to truncate</param>
        ///// <returns></returns>
        //public static long TruncateLeft(long n)
        //{
        //    return n % (int)Math.Pow(10, GetDigitCount(n) - 1);
        //}
        //public static int TruncateLeft(int n)
        //{
        //    return (int)TruncateLeft((long)n);
        //}
        //public static long[] TruncateLeftToArray(long n)
        //{
        //    var array = new long[GetDigitCount(n)];
        //    for (var i = 0; i < array.Length; i++)
        //    {
        //        array[i] = n;
        //        n = TruncateLeft(n);
        //    }
        //    return array;
        //}

        ///// <summary>
        ///// Truncates a number from right side [1234, 123, 12, 1, 0]
        ///// </summary>
        ///// <param name="n">Number to truncate</param>
        ///// <returns></returns>
        //public static long TruncateRight(long n)
        //{
        //    return n / 10;
        //}
        //public static int TruncateRight(int n)
        //{
        //    return (int)TruncateRight((long)n);
        //}
        //public static long[] TruncateRightToArray(long n)
        //{
        //    var array = new long[GetDigitCount(n)];
        //    for (var i = 0; i < array.Length; i++)
        //    {
        //        array[i] = n;
        //        n = TruncateRight(n);
        //    }
        //    return array;
        //}

        //public static long GetFirstDigit2(long n)
        //{
        //    return n/(int) Math.Pow(10, GetDigitCount(n) - 1);
        //}
        //public static long GetLastDigit2(long n)
        //{
        //    return n % 10;
        //}

        //public static long[] GetAllDigits(long n)
        //{
        //    var array = new long[GetDigitCount(n)];
        //    for (var i = array.Length-1; i >= 0; i--)
        //    {
        //        array[i] = GetLastDigit(n);
        //        n = TruncateRight(n);
        //    }
        //    return array;
        //}

        //public static bool IsPalindrome(int n)
        //{
        //var d = GetAllDigits(n);
        //var middle = d.Length / 2.0;
        //var isEven = d.Length % 2 == 0;

        //var b2 = isEven ? middle : Math.Floor(middle);
        //var b1 = isEven ? middle - 1 : middle;

        //var c1 = 0;
        //var c2 = d.Length - 1;
        //    while (c1 <= b1 && c2 >= b2)
        //    {
        //        if (d[c1] != d[c2]) return false;
        //        c1++;
        //        c2--;
        //    }
        //    return true;
        //}g3oz1k667ucv


        public static int GetLength(long n)
        {
            if (n == 0) return 0;
            return (int)Math.Floor(Math.Log10(n) + 1);
        }
        public static long TruncLeft(long n)
        {
            return (long)( n % Math.Pow(10, GetLength(n) - 1) );
        }
        public static long TruncLeft(long n, int amount)
        {
            for (var i = 0; i < amount; i++)
                n = TruncLeft(n);
            return n;
        }
        public static long TruncRight(long n)
        {
            return n / 10;
        }
        public static long TruncRight(long n, int amount)
        {
            for (var i = 0; i < amount; i++)
                n = TruncRight(n);
            return n;
        }
        public static long GetFirstDigit(long n)
        {
            return (long)(n / Math.Pow(10, GetLength(n) - 1));
        }
        public static long GetLastDigit(long n)
        {
            return n % 10;
        }
        public static long[] Explode(long n)
        {
            var array = new long[GetLength(n)];
            for (var i = array.Length - 1; i >= 0; i--)
            {
                array[i] = GetLastDigit(n); // get last digit
                n = TruncRight(n); //truncate right
            }
            return array;
        }
        public static long Implode(long[] n)
        {
            var pow = (int) Math.Pow(10, n.Length - 1);
            var i = 0;
            long num = 0;
            while (pow > 0)
            {
                num += n[i] * pow;
                i++;
                pow /= 10;
            }
            return num;
        }

        public static bool IsPalindrome(long n)
        {
            var d = Explode(n);
            var middle = d.Length / 2.0;
            var isEven = d.Length % 2 == 0;

            var b2 = isEven ? middle : Math.Floor(middle);
            var b1 = isEven ? middle - 1 : middle;

            var c1 = 0;
            var c2 = d.Length - 1;
            while (c1 <= b1 && c2 >= b2)
            {
                if (d[c1] != d[c2]) return false;
                c1++;
                c2--;
            }
            return true;
        }
    }
}