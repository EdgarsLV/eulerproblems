﻿// Euler 2
let stopWatch = System.Diagnostics.Stopwatch.StartNew()

let f n = Seq.unfold (fun state ->
        if (snd state > n) then None
        else Some(fst state + snd state, (snd state, fst state + snd state))) (1,1)
        |> Seq.where (fun x->x % 2=0)
        |> Seq.sum
    
printfn "Result: %i" (f 4000000)
stopWatch.Stop()
printfn "Execution time %fms" stopWatch.Elapsed.TotalMilliseconds
System.Console.ReadKey() |> ignore