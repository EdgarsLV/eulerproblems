﻿// Euler 4
let stopWatch = System.Diagnostics.Stopwatch.StartNew()

let getDigitCount n = if n = 0 then 1 else int (floor ((log10 (double n))+1.0))
let truncateLeft n = n % int (10.0**float ((getDigitCount n)-1))
let truncateRight n = n/10

let getFirstDigit n = n / int (10.0**float ((getDigitCount n)-1))
let getLastDigit n = n % 10


   

stopWatch.Stop()
printfn "Execution time %fms" stopWatch.Elapsed.TotalMilliseconds
System.Console.ReadKey() |> ignore