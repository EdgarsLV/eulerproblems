﻿using System;
using System.Collections.Generic;
using ConTools;

//Problem 10
//The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
//Find the sum of all the primes below two million.

namespace Euler10CS
{
    class Program10
    {
        static void Main(string[] args)
        {
            ConOut.MeasureTime(() =>
            {
                ConOut.Title("Euler 10");

                Tuple<List<int>, long> results = null;
                results = CalculateSum();
                ConOut.Title("COMPLETE", ConsoleColor.Cyan);
                ConOut.Print("Total primes: ", ConsoleColor.Magenta, false);
                ConOut.Print($"{results.Item1.Count}");
                ConOut.Print($"Result: {results.Item2}", ConsoleColor.Green);
                ConOut.AreEqual(142913828922, results.Item2);
            });
            Console.ReadKey();
        }

        public static List<int> SieveEratosthenes(int max)
        {
            var vals = new List<int>((int)(max / (Math.Log(max) - 1.08366)));
            var maxSquareRoot = Math.Sqrt(max);
            var eliminated = new System.Collections.BitArray(max + 1);

            vals.Add(2);

            for (int i = 3; i <= max; i += 2)
            {
                if (!eliminated[i])
                {
                    if (i < maxSquareRoot)
                    {
                        for (int j = i * i; j <= max; j += 2 * i)
                            eliminated[j] = true;
                    }
                    vals.Add(i);
                }
            }
            return vals;
        }

        public static Tuple<List<int>, long> CalculateSum(int range = 2000000)
        {
            var sieve = SieveEratosthenes(range);
            long sum = 0;
            foreach (var n in sieve)
                sum += n;
            return new Tuple<List<int>, long>(sieve, sum);
        }

    }
}
