﻿using System;
using System.Numerics;
using ConTools;

//Problem 20
//n! means n × (n − 1) × ... × 3 × 2 × 1
//For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
//and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
//Find the sum of the digits in the number 100!

namespace Euler20CS
{
    class Program20
    {
        static void Main(string[] args)
        {
            ConOut.MeasureTime(() =>
            {
                ConOut.Title("Euler 20");
                var results = FindResults();
                ConOut.Title("COMPLETE", ConsoleColor.Cyan);
                ConOut.Print("Factorial: ", ConsoleColor.Magenta);
                ConOut.Print($"{results.Item1}");
                ConOut.Print($"Sum result: {results.Item2}", ConsoleColor.Green);
                ConOut.AreEqual(648, results.Item2);
            });


            Console.ReadKey();
        }

        public static BigInteger Fact(BigInteger n)
        {
            BigInteger r = 1;
            for (BigInteger i = 1; i <= n; i++)
            {
                r *= i;
            }
            return r;
        }
        public static int SumDigits(BigInteger n)
        {
            var str = n.ToString();
            var sum = 0;
            foreach (var c in str)
            {
                sum += (int)char.GetNumericValue(c);
            }
            return sum;
        }

        public static Tuple<BigInteger, int> FindResults()
        {
            var fact = Fact(100);
            var sum = SumDigits(fact);

            return new Tuple<BigInteger, int>(fact, sum);
        }
    }
}
