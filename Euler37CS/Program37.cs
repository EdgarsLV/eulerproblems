﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConTools;

// Problem 37
// The number 3797 has an interesting property.Being prime itself, 
// it is possible to continuously remove digits from left to right, and remain prime at each stage: 3797, 797, 97, and 7. 
// Similarly we can work from right to left: 3797, 379, 37, and 3.
// Find the sum of the only eleven primes that are both truncatable from left to right and right to left.
// NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.

namespace Euler37CS
{
    class Program37
    {

        private static void Main(string[] args)
        {
            ConOut.MeasureTime(() =>
            {
                ConOut.Title("Euler 37");
                var results = Calculate();
                ConOut.Title("COMPLETE", ConsoleColor.Cyan);
                ConOut.Print("Primes: ", ConsoleColor.Magenta, false);
                ConOut.Print(string.Join(" ", results.Item1.Select(x => x.ToString()).ToArray()));
                ConOut.Print($"Sum: {results.Item2}", ConsoleColor.Green);
                ConOut.AreEqual(748317, results.Item2);
            });
            Console.ReadKey();
        }

        public static bool IsPrime(int n, HashSet<int> sieve)
        {
            return sieve.Contains(n);
        }
        public static bool IsTrunRight(int n, HashSet<int> sieve)
        {
            if (n < 10) return false;
            while (n != 0)
            {
                if (!IsPrime(n, sieve)) return false;
                n /= 10;
            }
            return true;
        }
        public static bool IsTrunLeft(int n, HashSet<int> sieve)
        {
            if (n < 10) return false;
            var rn = 0;
            var i = 10;
            while (rn < n)
            {
                rn = n % i;
                if (!IsPrime(rn, sieve)) return false;
                i *= 10;
            }
            return true;
        }

        // http://digitalbush.com/2010/02/26/sieve-of-eratosthenes-in-csharp/
        public static List<int> SieveEratosthenes(int max)
        {
            var vals = new List<int>((int)(max / (Math.Log(max) - 1.08366)));
            var maxSquareRoot = Math.Sqrt(max);
            var eliminated = new System.Collections.BitArray(max + 1);

            vals.Add(2);

            for (int i = 3; i <= max; i += 2)
            {
                if (!eliminated[i])
                {
                    if (i < maxSquareRoot)
                    {
                        for (int j = i * i; j <= max; j += 2 * i)
                            eliminated[j] = true;
                    }
                    vals.Add(i);
                }
            }
            return vals;
        }

        public static Tuple<List<int>, int> Calculate()
        {
            var sieve = new HashSet<int>(SieveEratosthenes(1000000));
            var primes = sieve.Where(x => IsTrunLeft(x, sieve) && IsTrunRight(x, sieve)).ToList();
            var sum = primes.Sum();
            return new Tuple<List<int>, int>(primes, sum);
        }
    }
}
