﻿using System;
using System.Collections.Generic;
using System.Text;
using ConTools;

//Problem 40
//An irrational decimal fraction is created by concatenating the positive integers:
//0.123456789101112131415161718192021...
//It can be seen that the 12th digit of the fractional part is 1.
//If dn represents the nth digit of the fractional part, find the value of the following expression.
//d1 × d10 × d100 × d1000 × d10000 × d100000 × d1000000

namespace Euler40CS
{
    class Program40
    {
        public static List<int> Numbers = new List<int>(){1, 10, 100, 1000, 10000, 100000, 1000000};


        static void Main(string[] args)
        {
            ConOut.MeasureTime(() =>
            {
                ConOut.Title("Euler 40");
                var r = GetResultNew();

                ConOut.Title("FINISHED", ConsoleColor.Cyan);
                ConOut.Print("Result: ", ConsoleColor.White, false);
                ConOut.Print(r, ConsoleColor.Green);
            });
            Console.ReadKey();
        }

        public static int GetResultNew()
        {
            var result = 1;
            var totalDigitCountSoFar = 0;
            var currentNumIndex = 0;
            for (var i = 1; i < int.MaxValue; i++)
            {
                var digitCount = Num.GetDigitCount(i);
                var totalSoFar = totalDigitCountSoFar + digitCount;

                if (totalSoFar >= Numbers[currentNumIndex])
                {
                    var index = digitCount - (totalSoFar - Numbers[currentNumIndex]) - 1;
                    result *= (int)Num.GetAllDigits(i)[index];
                    currentNumIndex++;
                }

                totalDigitCountSoFar += digitCount;
                if (currentNumIndex >= Numbers.Count) break;
            }

            return result;
        }
    }
}
