﻿using System;
using System.Linq;
using ConTools;

//Problem 41
//We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once.For example, 2143 is a 4-digit pandigital and is also prime.
//What is the largest n-digit pandigital prime that exists?

namespace Euler41CS
{
    class Program41
    {
        static void Main(string[] args)
        {
            ConOut.MeasureTime(() =>
            {
                var sieve = Primes.SieveEratosthenes(10000000);
                var pans = sieve.Where(IsPandigital);
                ConOut.Print(pans.Last());
            });

            Console.ReadKey();
        }

        public static bool IsPandigital(int num)
        {
            var minDig = 1;
            var maxDig = Num.GetDigitCount(num);
            if (maxDig > 9) maxDig = 9;
            var allDigs = Num.GetAllDigits(num);

            for (var i = minDig; i <= maxDig; i++)
            {
                if (allDigs.Count(x => x == i) > 1 || !allDigs.Contains(i)) return false;
            }
            return true;
        }
    }
}
