﻿using System;
using System.IO;
using System.Linq;
using ConTools;

namespace Euler42CS
{
    class Program42
    {
        static void Main(string[] args)
        {
            ConOut.MeasureTime(() =>
            {
                ConOut.Print("─── Euler 37 ───");
                ConOut.Print("Triangle words: ", ConsoleColor.White, false);
                var answer = GetAnswer();
                ConOut.Print(answer, ConsoleColor.Green);
                ConOut.AreEqual(162, answer);
            });
            Console.ReadKey();
        }

        public static bool NumberIsTriangle(double n)
        {
            var inv = 0.5*(-1.0 + Math.Sqrt(8.0*n + 1.0));
            return inv % 1.0 == 0.0;
        }
        public static bool WordIsTriangle(string word)
        {
            var sum = 0;
            foreach (var c in word)
            {
                sum += c - 'A' + 1;
            }
            return NumberIsTriangle(sum);
        }
        public static int GetAnswer()
        {
            return File.ReadAllText("p042_words.txt")
                .Replace("\"", "")
                .Split(',')
                .Where(WordIsTriangle)
                .Count();
        }
    }
}
