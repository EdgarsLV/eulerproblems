﻿// Euler 42
let stopWatch = System.Diagnostics.Stopwatch.StartNew()
let isTriangleNumber n =
    let inv = 0.5*(-1.0+(sqrt (8.0*n+1.0)))
    if inv % 1.0 = 0.0 then true else false

let charIndex (c : char) = (int c) - (int 'A') + 1

let parseFile = System.IO.File.ReadAllText("p042_words.txt").Replace("\"","").Split(',')

let wordIsTriangle (word : string) = 
    let sum = word |> Seq.sumBy(fun x->charIndex x)
    if isTriangleNumber(float sum) then true else false

let answer = parseFile |> Seq.filter(fun x-> wordIsTriangle x) |> Seq.length

printfn "Euler 42\nTriangle words: %A" answer
stopWatch.Stop()
printfn "Execution time %fms" stopWatch.Elapsed.TotalMilliseconds
System.Console.ReadKey() |> ignore