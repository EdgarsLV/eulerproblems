﻿using System;
using System.Linq;
using System.Collections.Generic;
using ConTools;

//Problem 4
//A palindromic number reads the same both ways.The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
//Find the largest palindrome made from the product of two 3-digit numbers.

namespace Euler4CS
{
    class Program4
    {
        static void Main(string[] args)
        {
            ConOut.MeasureTime(() =>
            {
                ConOut.Title("Euler 4");
                var results = Answer();
                ConOut.Title("COMPLETE", ConsoleColor.Cyan);
                ConOut.Print($"Product: {results}", ConsoleColor.Green);
                ConOut.AreEqual(906609, results);
            });


            Console.ReadKey();
        }

        public static bool IsPalindrome(int n)
        {
            var d = Num.GetAllDigits(n);
            var middle = d.Length/2.0;
            var isEven = d.Length%2 == 0;

            var b2 = isEven ? middle : Math.Floor(middle);
            var b1 = isEven ? middle - 1 : b2;

            var c1 = 0;
            var c2 = d.Length - 1;
            while (c1 <= b1 && c2 >= b2)
            {
                if (d[c1] != d[c2]) return false;
                c1++;
                c2--;
            }
            return true;
        }

        public static int Answer()
        {
            var pals = new List<int>();
            for (var i = 999; i >= 100; i--)
            {
                for (var j = 999; j >= 100; j--)
                {
                    var ij = i * j;
                    if (IsPalindrome(ij))
                    {
                        pals.Add(ij);
                    }
                }
            }
            return pals.Max();
        }
    }
}
