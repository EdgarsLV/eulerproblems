﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConTools;

namespace Euler5CS
{
    class Program5
    {
        static void Main(string[] args)
        {

            ConOut.Print(Smallest(20));
            Console.ReadKey();
        }

        public static int Smallest(int uprange)
        {
            for (var i = 1; i < int.MaxValue; i++)
            {
                for (var j = 1; j <= uprange; j++)
                {
                    if (i%j != 0) break;
                    if (j == uprange && i % j == 0) return i;
                }
            }
            return -1;
        }
    }
}
