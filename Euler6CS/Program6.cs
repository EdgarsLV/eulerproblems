﻿using System;
using System.Collections.Generic;
using ConTools;

//Problem 6
//The sum of the squares of the first ten natural numbers is,
//12 + 22 + ... + 102 = 385
//The square of the sum of the first ten natural numbers is,
//(1 + 2 + ... + 10)2 = 552 = 3025
//Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.
//Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.

namespace Euler6CS
{
    class Program6
    {
        static void Main(string[] args)
        {

            ConOut.MeasureTime(() =>
            {
                ConOut.Title("Euler 6");
                var result = Difference(100);
                ConOut.Title("COMPLETE", ConsoleColor.Cyan);
                ConOut.Print($"Result: {result}", ConsoleColor.Green);
                ConOut.AreEqual((long)25164150, result);
            });
            Console.ReadKey();
        }

        public static long SumOfSquares(int upper)
        {
            long sum = 0;

            for (int i = 1; i <= upper; i++)
                sum += i*i;

            return sum;
        }
        public static long SquareOfSums(int upper)
        {
            long sum = 0;
            for (int i = 1; i <= upper; i++)
                sum += i;
            return sum * sum;
        }
        public static long Difference(int upper)
        {
            return SquareOfSums(upper) - SumOfSquares(upper);
        }
    }
}
