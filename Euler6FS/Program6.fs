﻿// Euler 6
let stopWatch = System.Diagnostics.Stopwatch.StartNew()
let diff n = ([1..n] |> List.sum |> (fun x -> x*x)) - ([1..n] |> List.map (fun x -> x*x) |> List.sum)

printfn "Result: %i" (diff 100)
stopWatch.Stop()
printfn "Execution time %fms" stopWatch.Elapsed.TotalMilliseconds
System.Console.ReadKey() |> ignore