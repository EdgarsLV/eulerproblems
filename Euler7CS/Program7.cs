﻿using System;
using System.Collections.Generic;
using ConTools;

//By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
//What is the 10 001st prime number?

namespace Euler7CS
{
    class Program7
    {
        public static int PrimeOrderNum = 10001;
        static void Main(string[] args)
        {
            ConOut.MeasureTime(() =>
            {
                ConOut.Title("Euler 7");
                var prime = (int)FindPrime();
                ConOut.Title("COMPLETE", ConsoleColor.Cyan);
                ConOut.Print($"Result: {prime}", ConsoleColor.Green);
                ConOut.AreEqual(104743, prime);
            });

            Console.ReadKey();
        }

        public static long FindPrime(int n = 10001)
        {
            var sieve = SieveEratosthenes(1000000);
            return sieve[n-1]; // 0 index
        }

        public static List<int> SieveEratosthenes(int max)
        {
            var vals = new List<int>((int)(max / (Math.Log(max) - 1.08366)));
            var maxSquareRoot = Math.Sqrt(max);
            var eliminated = new System.Collections.BitArray(max + 1);

            vals.Add(2);

            for (int i = 3; i <= max; i += 2)
            {
                if (!eliminated[i])
                {
                    if (i < maxSquareRoot)
                    {
                        for (int j = i * i; j <= max; j += 2 * i)
                            eliminated[j] = true;
                    }
                    vals.Add(i);
                }
            }
            return vals;
        }

    }
}
