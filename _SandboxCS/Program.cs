﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConTools;

namespace _SandboxCS
{
    class Program
    {
        static void Main(string[] args)
        {

            //ConOut.MeasureTime(() => { TruncationTest(); });
            ConOut.Print(Num.Implode(Num.Explode(1234567890)));
            Console.ReadKey();
        }

        public static void TruncationTest(long startingnum = 1234567890)
        {
            ConOut.Print($"Truncation test {startingnum}");
            var count = Num.GetLength(startingnum);
            long n = startingnum;
            ConOut.Print($">>> {n}");
            for (int i = 0; i < count; i++)
            {
                n = Num.TruncLeft(n);
                ConOut.Print($">>> {n}");
            }
            n = startingnum;
            ConOut.Print($">>> {n}");
            for (int i = 0; i < count; i++)
            {
                n = Num.TruncRight(n);
                ConOut.Print($">>> {n}");
            }
        }
    }
}
